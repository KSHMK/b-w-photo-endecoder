import Image
class BWImage:
    filename = ""
    im = 0
    pix = 0
    x = 0
    y = 0
    def __init__(self):
        pass

    def openfile(self,filename=""):
        self.im = Image.open(filename)
        (self.x,self.y) = self.im.size
        self.filename = filename

    def newfile(self,filename="",x=0,y=0):
        self.im = Image.new("RGB",(x,y),(255,255,255))
        (self.x, self.y) = (x,y)
        self.filename = filename
        self.im.save(filename)
        
    def getxylen(self):
        return (self.x,self.y)

    def readpixel(self):
        pixbits = [[0 for col in range(self.x)] for row in range(self.y)]
        self.pix = self.im.load()
        for row in range(self.y):
            for col in range(self.x):
                if self.pix[col,row] == (0,0,0) or self.pix[col,row] == (0,0,0,255):
                    pixbits[row][col] = 1
                else:
                    pixbits[row][col] = 0
        return pixbits

    def writepixel(self,pixbits):
        self.pix = self.im.load()
        for row in range(self.y):
            for col in range(self.x):
                if pixbits[row][col] == 1:
                    self.pix[col,row] = (0,0,0)
                else:
                    self.pix[col,row] = (255,255,255)
        self.im.save(self.filename)      
        

