class ENDE:
    def __Init__(self):
        pass
    def InternalState(self,k):
        (x1,y1,x2,y2)=k
        bc = 0
        wc = 0
        for y in range(y1,y2+1):
            for x in range(x1,x2+1):
                if self.pix[y][x] == 1:
                    bc+=1
                else:
                    wc+=1
        if bc == 0:
            return 0
        elif wc == 0:
            return 1
        else:
            return 2
    def recenfuc(self,k):
        (x1,y1,x2,y2)=k
        px = [0,1,0,1]
        py = [0,0,1,1]
        its = self.InternalState(k)
        if its != 2:
            return str(its)
        ret = "/"
        tx = (x2-x1)/2
        ty = (y2-y1)/2
        ox = (x2-x1) % 2
        oy = (y2-y1) % 2
        for i in range(4):
            ret += self.recenfuc(((x1+((tx+1)*px[i])),(y1+((ty+1)*py[i])),((x1+x2)/2+((tx+ox)*px[i])),((y1+y2)/2+((ty+oy)*py[i]))))
        return ret
    def encode(self,pix,nx,ny):
        self.pix = pix
        encode = str(nx)+"/"+str(ny)+"/"
        encode += self.recenfuc((0,0,nx-1,ny-1))
        return encode

    def pixchanger(self,k):
        (x1,y1,x2,y2)=k
        if self.encode[self.count] == '/':
            self.count += 1
            return 1
        for y in range(y1,y2+1):
            for x in range(x1,x2+1):
                if self.encode[self.count] == '1':
                    self.pix[y][x]=1
                else:
                    self.pix[y][x]=0
        self.count += 1
        return 0
    def recdefuc(self,k):
        (x1,y1,x2,y2)=k
        px = [0,1,0,1]
        py = [0,0,1,1]
        its = self.pixchanger(k)
        if its != 1:
            return 
        tx = (x2-x1)/2
        ty = (y2-y1)/2
        ox = (x2-x1) % 2
        oy = (y2-y1) % 2
        for i in range(4):
            self.recdefuc(((x1+((tx+1)*px[i])),(y1+((ty+1)*py[i])),((x1+x2)/2+((tx+ox)*px[i])),((y1+y2)/2+((ty+oy)*py[i]))))
    def decode(self,encode):
        nx = int(encode[:encode.find("/")])
        encode = encode[encode.find("/")+1:]
        ny = int(encode[:encode.find("/")])
        encode = encode[encode.find("/")+1:]
        self.pix = [[0 for col in range(nx)] for row in range(ny)]
        self.encode = encode
        self.count = 0
        self.recdefuc((0,0,nx-1,ny-1))
        return (self.pix,nx,ny)
        
        
        
